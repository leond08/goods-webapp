## Goods Web Application
Sample Spring Boot web application for importing a csv file. 


## Requirements
Java 1.8

Gradle 1.4+

MySQL


## Importing the database
 `mysql -proot -u root goods < goods.sql`
 
 
## Building the application
`gradle clean build`


## Running the application
### Using Gradle Plugin
`cd /path/to/project`

`gradle bootRun`

### Running as packaged application
`java -jar build/libs/test-goods-webapp-0.1.0.jar`


## Change Log



## Issues
Current file limit 25MB