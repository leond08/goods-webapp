package com.goods;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Goods {
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	@Column(name="name")
	private String name;
	@Column(name="description")
	private String description;
	@Column(name="length")
	private Integer length;
	@Column(name="height")
	private Integer height;
	@Column(name="area")
	private Integer area;
	@Column(name="nr")
	private String nr;
	
	public Goods() {
		super();
	}
	
	public Goods(String name, String description, Integer length, Integer height, Integer area, String nr) {
		super();
		this.name = name;
		this.description = description;
		this.length = length;
		this.height = height;
		this.area = area;
		this.nr  = nr;
	}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length= length;
    }
    
    public Integer getHeight() {
        return height;
    }

    public void setHieght(Integer height) {
        this.height = height;
    }
    
    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }
    
    public String getNr() {
        return name;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }

}
