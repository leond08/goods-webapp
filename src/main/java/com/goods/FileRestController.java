package com.goods;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileRestController {
	
	private static String UPLOADED_FOLDER = System.getProperty("java.io.tmpdir");
	
	@Autowired
	private GoodsRepository goodsRepository;
	
	@Autowired
	private GoodsService goodsService;
	
	
	@RequestMapping(path="/goods", method=RequestMethod.GET)
	@Async
	public List<Goods> getAllEmployees(){
		return goodsService.getAllGoods();
	}
	
    @SuppressWarnings("unchecked")
	@PostMapping("/api/upload")
    public ResponseEntity<?> uploadFile(
            @RequestParam("file") MultipartFile uploadfile) {
  

        if (uploadfile.isEmpty()) {
            return new ResponseEntity("Please select a file!", HttpStatus.OK);
        }
        
    	String filename = uploadfile.getOriginalFilename();
    	int lastIndex = filename.lastIndexOf('.');
    	String substring = filename.substring(lastIndex, filename.length());

        if (!substring.equals(".csv")) {
        	return new ResponseEntity("File not supported - " +
                    uploadfile.getOriginalFilename(), HttpStatus.OK);
        }


        try {
        	
        	saveUploadedFiles(uploadfile);
            
        	File f = new File(Paths.get(UPLOADED_FOLDER +  "/"+ uploadfile.getOriginalFilename()).toString());
        	if(f.exists() && !f.isDirectory()) { 
        		csvParser(uploadfile);
        	}
        	else {
        		return new ResponseEntity("Uploaded file - " +
                        uploadfile.getOriginalFilename() + "not found", HttpStatus.NOT_FOUND); 
        	}
            

        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity("Successfully inserted data to database", new HttpHeaders(), HttpStatus.OK);

    }
    
    //save file
	private void saveUploadedFiles(MultipartFile file) throws IOException {
    	
        try {

            // Get the file and save it to tmp
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER +  "/"+ file.getOriginalFilename());
            Files.write(path, bytes);
            

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	@Async
	public void csvParser(MultipartFile file) {
		
		try {
			
		BufferedReader reader = Files.newBufferedReader(Paths.get(UPLOADED_FOLDER +  "/"+ file.getOriginalFilename()));
		CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
        		.withFirstRecordAsHeader()
        		.withIgnoreHeaderCase().withTrim());
        
        for (CSVRecord csvRecord: csvParser) {
        	
        	Goods goods = new Goods();
        	//Accessing the values by column header name
            String name = csvRecord.get("name");
            String desc = csvRecord.get("description");
            Integer strLength = Integer.parseInt(csvRecord.get("length"));
            Integer strHeigth = Integer.parseInt(csvRecord.get("height"));
            String nr	= csvRecord.get("nr");
            Integer area = strLength * strHeigth;
            
            goods.setName(name);
            goods.setLength(strLength);
            goods.setDescription(desc);
            goods.setHieght(strHeigth);
            goods.setArea(area);
            goods.setNr(nr);
            
            //Printing the record 
/*                System.out.println("Record Number - " + csvRecord.getRecordNumber());
            System.out.println("Name : " + name);
            System.out.println("Description : " + desc);
            System.out.println("Length : " + strLength);
            System.out.println("Height : " + strHeigth);
            System.out.println("Area "  + area);
            System.out.println("Nr : " + nr);
            System.out.println("\n\n");*/
            // Insert to database
            goodsRepository.save(goods);
              
        }

	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

}